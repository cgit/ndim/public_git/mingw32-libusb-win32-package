%define __strip %{_mingw32_strip}
%define __objdump %{_mingw32_objdump}
%define _use_internal_dependency_generator 0
%define __find_requires %{_mingw32_findrequires}
%define __find_provides %{_mingw32_findprovides}

%define		tarname libusb-win32-src

Name:		mingw32-libusb-win32
Version:	0.1.12.2
Release:	4%{?dist}
Summary:	Win32 port of libusb

Group:		Development/Libraries
# The source package also contains code under GPLv2+, but we do
# not ship it in the binary RPM.
License:	LGPLv2+
URL:		http://libusb-win32.sourceforge.net/
Source0:	http://prdownloads.sourceforge.net/libusb-win32/%{tarname}-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

# First filesystem to provide mingw32(setupapi.dll) and mingw32(cfgmgr32.dll)
BuildRequires:	mingw32-filesystem >= 40-3
Requires:	mingw32-filesystem >= 40-3

BuildRequires:	mingw32-binutils
BuildRequires:	mingw32-gcc
BuildRequires:	dos2unix

BuildArch:	noarch


%description
libusb-win32 is a port of the USB library libusb to the Windows
operating systems (Win98SE, WinME, Win2k, WinXP).

The library allows user space applications to access any USB
device on Windows in a generic way without writing any line of
kernel driver code.


%prep
%setup -q -n %{tarname}-%{version}


%build
export PATH=%{_mingw32_bindir}:$PATH
# Build is not smp safe
make host_prefix=%{_mingw32_target}
dos2unix AUTHORS.txt
dos2unix README.txt


%install
rm -rf "%buildroot"
#make bin_dist DESTDIR="%buildroot"
%{__install} -m 0755 -d "%{buildroot}%{_mingw32_bindir}"
%{__install} -m 0644 libusb0.def "%{buildroot}%{_mingw32_bindir}"
%{__install} -m 0755 libusb0.dll "%{buildroot}%{_mingw32_bindir}"
%{__install} -m 0755 -d "%{buildroot}%{_mingw32_includedir}"
%{__install} -m 0644 src/usb.h "%{buildroot}%{_mingw32_includedir}"


%clean
rm -rf "%buildroot"


%files
%defattr(-,root,root,-)
%doc AUTHORS.txt README.txt
%{_mingw32_bindir}/libusb0.def
%{_mingw32_bindir}/libusb0.dll
%{_mingw32_includedir}/usb.h


%changelog
* Sun Jan  3 2010 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.12.2-4
- Update to 0.1.12.2 release

* Sun Jan  3 2010 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.12.1-4
- Rework package to build on F11

* Thu Feb 19 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.12.1-3
- ship libusb0.def alongside libusb0.dll

* Thu Feb 19 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.12.1-2
- require mingw32-filesystem providing cfgmgr32.dll and setupapi.dll
- use mktemp version of BuildRoot

* Wed Feb 18 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.12.1-1
- initial packaging
